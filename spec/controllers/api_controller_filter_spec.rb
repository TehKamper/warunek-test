require 'rails_helper'
require 'json'

RSpec.describe ApiController, type: :controller do
  describe 'POST add_coins' do
    it 'is authenticated by HTTP Basic and access token' do
      @user = FactoryGirl.create(:user)
      @user.generate_accesstoken!

      post :add_coins, amount: 20
      expect(response.body).to eq("HTTP Basic: Access denied.\n")

      username = 'apiusr'
      password = 'V4r00n3q'
      request.env['HTTP_AUTHORIZATION'] =
        ActionController::HttpAuthentication::Basic.encode_credentials(
          username,
          password
        )

      post :add_coins, amount: 20
      api_response = JSON.parse(response.body, symbolize_names: true)
      expect(api_response[:status]).to eq(401)
      expect(api_response[:body][:error]).to eq('Not authorized')

      request.headers['X-Authorization'] = "Token=#{AccessToken.first.token}"

      post :add_coins, amount: 20
      api_response = JSON.parse(response.body, symbolize_names: true)
      expect(api_response[:status]).to eq(200)
    end
  end
end
