require 'rails_helper'
require 'json'

RSpec.describe ApiController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @university = University.create(
      name: "Uniwersytet Jagielloński w Krakowie",
      slug: "UJK",
      public: true
    )
    username = 'apiusr'
    password = 'V4r00n3q'
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(username, password)
  end

  describe 'POST login' do
    it 'logs user in' do
      post :login, email: 'john@doe.com', password: 'testtest'
      api_response = JSON.parse(response.body, symbolize_names: true)
      expect(api_response[:status]).to eq(200)
      expect(api_response[:body][:user][:email]).to eq(@user.email)
      expect(api_response[:body][:user][:first_name]).to eq(@user.first_name)
      expect(api_response[:body][:user][:last_name]).to eq(@user.last_name)
    end
  end

  describe 'POST register' do
    it 'registers user' do
      post :register, {
        email: 'test@test.com',
        password: 'testtest',
        first_name: 'Testus',
        last_name: 'Testerre',
        username: 'Testnick',
        gender: 'M',
        university: 'UJK'
      }
      api_response = JSON.parse(response.body, symbolize_names: true)
      expect(api_response[:status]).to eq(200)
      expect(api_response[:body][:user][:email]).to eq('test@test.com')
      expect(api_response[:body][:user][:first_name]).to eq('Testus')
      expect(api_response[:body][:user][:last_name]).to eq('Testerre')
      expect(api_response[:body][:user][:university][:slug]).to eq('UJK')
      expect(api_response[:body][:user][:gender]).to eq('M')
    end
  end

  describe 'POST add_coins' do
    it 'adds coins' do

    end
  end
end
