require 'rails_helper'

RSpec.describe User, type: :model do
  it 'should generate accesstoken' do
    @user = FactoryGirl.create(:user)
    token = @user.generate_accesstoken!
    expect(@user.access_tokens).to include(token)
  end

  it 'should store no more than 10 access_tokens' do
    expect(User::MAX_USER_TOKENS).to eq(10)
    @user = FactoryGirl.create(:user)

    token = @user.generate_accesstoken!

    (User::MAX_USER_TOKENS - 1).times do
      @user.generate_accesstoken!
    end
    expect(@user.access_tokens.first).to eq(token)

    token = @user.generate_accesstoken!
    expect(@user.access_tokens.first).not_to eq(token)

    expect(@user.access_tokens.count).to eq(10)
  end
end
