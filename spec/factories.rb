FactoryGirl.define do
  factory :access_token do
    user_id 1
    token "MyString"
  end

  factory :user do
    first_name 'John'
    last_name 'Doe'
    email 'john@doe.com'
    password 'testtest'
    password_confirmation 'testtest'
  end
end
