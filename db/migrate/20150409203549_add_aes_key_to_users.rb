class AddAesKeyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :aes_key, :string
  end
end
