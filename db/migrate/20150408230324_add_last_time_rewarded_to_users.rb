class AddLastTimeRewardedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_time_rewarded, :datetime
  end
end
