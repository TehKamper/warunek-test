class ReworkGenders < ActiveRecord::Migration
  def change
    remove_column :users, :gender_id
    remove_table :genders
    add_column :users, :gender, :string, limit: 1
  end
end
