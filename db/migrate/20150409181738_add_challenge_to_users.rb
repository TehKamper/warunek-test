class AddChallengeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :challenge, :integer
  end
end
