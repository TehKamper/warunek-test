class CreateGenders < ActiveRecord::Migration
  def change
    create_table :genders do |t|
      t.string :gender

      t.timestamps
    end
    Gender.create(:gender => 'M')
    Gender.create(:gender => 'K')
  end
end
