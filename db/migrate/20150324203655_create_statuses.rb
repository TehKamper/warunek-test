class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :name
    end
    Status.create(:name => 'Student')
    Status.create(:name => 'Starosta')
    Status.create(:name => 'Magister')
    Status.create(:name => 'Doktorant')
    Status.create(:name => 'Dziekan')
  end
end
