class AddLevelAndStatusToUsers < ActiveRecord::Migration
  def change
    add_column :users, :level_id, :integer, :default => 1
    add_column :users, :status_id, :integer, :default => 1
  end
end
