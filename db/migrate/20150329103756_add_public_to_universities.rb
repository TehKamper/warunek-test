class AddPublicToUniversities < ActiveRecord::Migration
  def change
    add_column :universities, :public, :boolean
  end
end
