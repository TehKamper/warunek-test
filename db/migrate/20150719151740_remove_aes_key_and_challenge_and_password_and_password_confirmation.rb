class RemoveAesKeyAndChallengeAndPasswordAndPasswordConfirmation < ActiveRecord::Migration
  def up
    remove_column :users, :aes_key
    remove_column :users, :challenge
    remove_column :users, :password
    remove_column :users, :password_confirmation
  end

  def down
    add_column :users, :aes_key, :string
    add_column :users, :challenge, :integer
    add_column :users, :password, :string
    add_column :users, :password_confirmation, :string
  end
end
