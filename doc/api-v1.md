Zbieram na Warunek
==================
API specification
----------------
######Version 0.1a

######Version number 1

---

## Used standards
TCP/IP

HTTP / HTTPS - RFC 1738 - https://tools.ietf.org/html/rfc1738

JSON - RFC 7159 - https://tools.ietf.org/html/rfc7159

---

## Endpoints

All endpoints should be in following format:
```
<protocol>://<hostname>[:port]/api<version_number>/<resource>.<format>[?<parameters>]
```
where `<parameters>` are in format of `<key>=<value>[&<otherkey>=<othervalue[<parameters>]]`

exapmles:  
https://warunek.zbieramna.example.com:8080/api7/feed.json
http://warunek.zbieramna.example.com/api13/statistics.json?game_id=5&page=2

## Login

### Challenge Request
URL
```
/api1/login.json
```

Example Request

```
GET /api1/login.json?email=przemek@example.com
```

Example Response
```
200 OK

{
  "challenge": 1337
}
```
```
200 OK

{
  "error": "Invalid email."
}
```

### Challenge Response
URL
```
GET /api1/login.json
```
Example Request
```
POST /api1/login.json?email=przemek@example.com

{
  "challenge_response": 1338
}
```

Example Response
```
200 OK

{
  "token": "cHl0aG9uIHJvY2tzIQ="
}
```
```
200 OK

{
  "error": "Invalid password."
}
```

## Register
TODO

## Statistics
TODO

## Coin collect
TODO
