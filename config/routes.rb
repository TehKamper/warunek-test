Rails.application.routes.draw do
  devise_scope :user do
    authenticated :user do
      root :to => 'static_pages#coins_collector', :as => :collector
    end
    unauthenticated :user do
      root :to => 'static_pages#welcome', :as => :welcome
    end
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations" }

  resources :conversations do
    resources :messages
  end

  resources :password_resets

  resources :users
  post '/user/:user_id/edit' => 'users#edit_user', as: 'edit_user_prompt'

  post '/add_coin' => 'static_pages#add_coin', as: 'add_coin'

  post '/api/add_coins' => 'api#add_coins', as: 'add_coins'
  get '/api/challenge' => 'api#challenge', as: 'api_challenge'
  get '/api/log_in' => 'api#log_in', as: 'api_log_in'
  get '/api/sync' => 'api#sync', as: 'api_sync'
  get '/api/request_key' => 'api#request_key', as: 'api_request_key'
  get '/api/register' => 'api#register', as: 'api_register'

  post '/set_info_cookie' => 'application#set_info_cookie', as: 'set_info_cookie'

  #new routes
  scope :api do
    post '/login' => 'api#login', as: 'login'
  end
end
