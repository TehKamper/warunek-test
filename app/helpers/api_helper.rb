module ApiHelper
  def self.user_info user, options=nil
    json = {}
    json[:user] = user.as_json(except: [
      :id,
      :created_at,
      :updated_at,
      :provider,
      :uid,
      :gender_id,
      :university_id,
      :last_time_rewarded,
      :avatar_file_name,
      :avatar_file_size,
      :avatar_updated_at,
      :avatar_content_type,
      :password_reset_token,
      :password_reset_sent_at
    ])
    if options.present?
      if options[:include_university]
        json[:user][:university] = self.university_info(user.university)
      end
    end
    return json
  end

  def self.university_info university, options=nil
    json = university.as_json(only: [
      :name,
      :slug
    ])
    return json
  end
end
