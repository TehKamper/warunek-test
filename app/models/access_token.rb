class AccessToken < ActiveRecord::Base
  belongs_to :user

  def self.generate user_id
    user = User.find(user_id)
    user.access_tokens.first.destroy if user.access_tokens.count == 10
    token = AccessToken.create(user_id: user_id, token: SecureRandom.hex(32))
    user.access_tokens << token
    token
  end
end
