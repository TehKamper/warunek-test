class User < ActiveRecord::Base
  require 'securerandom'

  before_create :assign_uuid

  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :trackable, :validatable, :omniauthable,
         omniauth_providers: [:facebook]

  belongs_to :level
  belongs_to :status
  belongs_to :university

  has_many :conversations, foreign_key: :sender_id
  has_many :access_tokens

  has_attached_file :avatar, styles:
  {
    thumb: '100x100>',
    square: '200x200#',
    medium: '300x300>'
  }

  validates_attachment_content_type :avatar, content_type: %r{\Aimage\/.*\Z}
  validates_presence_of :first_name, :last_name

  COIN_AMOUNT = [5]
  MAX_USER_TOKENS = 10

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.first_name = auth.info.name.split[0]
      user.last_name = auth.info.name.split[1]
      user.avatar = auth.info.image
    end
  end

  def name
    "#{first_name} #{last_name}"
  end

  def login
    (username.nil? ? name : username)
  end

  def will_receive_bonus?
    date = Time.now
    time_rewarded = Time.local(date.year, date.month, date.day)
    if last_time_rewarded.nil? || (time_rewarded != last_time_rewarded)
      self.last_time_rewarded = time_rewarded
      self.coins = coins.to_i + COIN_AMOUNT[0]
      save
      return true
    else
      return false
    end
  end

  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  # new methods

  def generate_accesstoken!
    token = AccessToken.generate(self.id)
    access_tokens << token
    token
  end

  private

  def assign_uuid
    uuid = SecureRandom.hex(32)
    while User.where(:uuid => uuid).count > 0 do
      uuid = SecureRandom.hex(32)
    end
    self.uuid = uuid
    true
  end

  def generate_auth_token
    begin
      self.auth_token = SecureRandom.urlsafe_base64
    end while User.exists?(:auth_token => self.auth_token)
  end
end
