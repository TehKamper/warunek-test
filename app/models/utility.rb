class Utility
  require 'csv'
  require 'securerandom'

  def self.parse_uni_csv path, is_public
    file = File.open(path)
    CSV.foreach(file.path, headers: true) do |row|
      hash = row.to_hash
      University.create(:name => hash['name'], :slug => hash['code'], :public => is_public)
    end
  end

  def self.assign_uuid
    User.where('uuid is null').each do |user|
      user.uuid = SecureRandom.hex(32)
      user.save
    end
  end
end
