class UserMailer < ActionMailer::Base
  default from: "support@zbieramnawarunek.pl"

  def password_reset(user)
    @user = user
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Przypomnienie hasła")
  end
end
