class StaticPagesController < ApplicationController

  def welcome
    if params[:register].present?
      @register = true
    end
    if params[:token].present?
      @change_password = true
      @user = User.find_by_password_reset_token(params[:token])
    else
      @user = User.new
    end
  end
  
  def coins_collector
    @user = current_user 
    @users = User.all - [current_user]
  end

  def add_coin
    @user = current_user
    @user.coins = @user.coins.to_i + 1
    @user.save
    head :ok
  end
end
