class PasswordResetsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    user.send_password_reset if user
  	redirect_to welcome_path
  end

  def edit
  	@user = User.find_by_password_reset_token(params[:id])
    @reset_password = true
    redirect_to :controller => 'static_pages', :action => 'welcome', :token => @user.password_reset_token
  end

  def update
  	@user = User.find_by_id(params[:id])
  	@user.password = params[:user][:password]
  	@user.password_confirmation = params[:user][:password_confirmation]
  	@user.save
  	redirect_to welcome_path
  end
end
