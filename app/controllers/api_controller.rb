class ApiController < ApplicationController

  before_filter :authenticate
  before_filter :check_access_token, except: [
    :register,
    :login
  ]

  skip_before_filter  :verify_authenticity_token

  require "securerandom"

  def add_coins
    @amount = params[:amount]
    @auth_user.coins = @auth_user.coins.to_i + @amount.to_i
    if @auth_user.save
      json = {
        status: 200,
        body: ApiHelper.user_info(@auth_user)
      }
    else
      json = {
        status: 500,
        error: 'unable to save coins amount'
      }
    end
    render json: json
  end

  def register
    @user = User.new
    @user.email = params[:email]
    @user.password = params[:password]
    @user.password_confirmation = params[:password]
    @user.first_name = params[:first_name]
    @user.last_name = params[:last_name]
    @user.username = params[:username]
    @user.gender = params[:gender]
    @user.university = University.find_by slug: params[:university]
    if @user.save
      @user.generate_accesstoken!
      json = {
        status: 200,
        body: ApiHelper.user_info(@user, include_university: true)
      }
    else
      json = {
        error: 'unable to register user',
        status: 500
      }
    end
    render json: json
  end

  def login
    @user = User.find_by email: params[:email]
    if @user.nil?
      json = {
        error: 'no such user',
        status: 500
      }
    elsif !@user.valid_password?(params[:password])
      json = {
        error: 'invalid password',
        status: 500
      }
    else
      @user.generate_accesstoken!
      json = {
        status: 200,
        body: ApiHelper.user_info(@user, include_university: true)
      }
    end
    render json: json
  end

  private

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "apiusr" && password == "V4r00n3q"
    end
  end

  def check_access_token
    header = request.headers["X-Authorization"]
    token_header = /Token=[\S]+/.match(header)
    @header_token = token_header[0].split('=')[1] if token_header.present?
    if @header_token.nil?
      render json: { body: { error: "Not authorized" }, status: 401 }
    else
			@access_token = AccessToken.find_by token: @header_token if @header_token.present?
      @auth_user = @access_token.user if @access_token.present?
      if @access_token.nil? || @auth_user.nil?
        render json: { body: { error: "Invalid accesstoken" }, status: 500 }
      end
    end
  end
end
