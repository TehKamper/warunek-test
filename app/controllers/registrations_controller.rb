class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    @university = University.find_by_name(params[:user][:university_id])
    if @user = User.create(user_params)
      @user.university_id = @university.id if @university.present?
      @user.save
      sign_in(@user)
      redirect_to collector_path
    else
      redirect_to new_user_path
    end
  end

  def update
    super
  end

  private
  
  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :gender_id, :university_id, :email, :password, :password_confirmation)
  end
end 
