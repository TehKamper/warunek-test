class ApplicationController < ActionController::Base

  before_filter :check_cookie_info

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def set_info_cookie
    cookies.permanent[:info_read] = 'true'
    head :ok
  end

  private

  def check_cookie_info
    if cookies[:info_read] != 'true'
      @display_cookie_info = true
    else
      @display_cookie_info = false
    end
  end

  def allow_iframe
    response.headers['X-Frame-Options'] = 'ALLOW-FROM www.zbieramnawarunek.pl'
  end
end
