class UsersController < ApplicationController

  before_action :authenticate_user!

  def edit_user
    @user = User.find_by_id(params[:user_id])
    respond_to do |format|
      format.js
    end
  end

  def update
  	@user = current_user
  	@university = University.find_by_name(params[:user][:university_id])
  	@user.first_name = params[:user][:first_name] if params[:user][:first_name].present?
  	@user.last_name = params[:user][:last_name] if params[:user][:last_name].present?
  	@user.email = params[:user][:email] if params[:user][:email].present?
  	@user.gender_id = params[:user][:gender_id] if params[:user][:gender_id].present?
  	@user.password = params[:user][:password] if params[:user][:password].present?
  	@user.password_confirmation = params[:user][:password_confirmation] if params[:user][:password_confirmation].present?
    @user.university_id = @university.id if @university.present?
    @user.save
  	redirect_to collector_path
   	end
end